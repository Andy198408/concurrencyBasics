package fundamentals.threadGroup;

public class ThreadGroupExample {
    public static void main(String[] args) throws InterruptedException {

        ThreadGroup group = new ThreadGroup("GroupA");

        new Task(group, "A").start();
        new Task(group, "B").start();
        new Task(group, "C").start();
        new Task(group, "D").start();

        Thread.sleep(5000);

        group.interrupt();




    }
}
