package fundamentals.threadexample;

public class ThreadInterruptJoin implements Runnable {

    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("This is message #" + i);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                System.out.println("I'm about to stop");
                return;
            }
        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new ThreadInterruptJoin());
        t1.start();

        try {

            t1.join();  //this causes thread main to wait on t1

        } catch (InterruptedException ex) {
            // do nothing
        }

        System.out.println("I'm " + Thread.currentThread().getName());
    }
}
