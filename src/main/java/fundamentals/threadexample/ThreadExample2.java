package fundamentals.threadexample;

public class ThreadExample2 implements Runnable {

    @Override
    public void run() {
        System.out.println("My name is runnable check: " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Runnable task = new ThreadExample2();
        Thread t2 = new Thread(task);
        t2.start();
        t2.setName("runnable");
        System.out.println("My name is main check: " + Thread.currentThread().getName());

        System.out.println(t2.getClass());
        System.out.println(t2.getState());
    }
}
