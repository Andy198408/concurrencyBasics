package synchronization.atomicVariables.sync;

public class Counter {
    private int value;

    public synchronized void increment() {
        value++;
    }

    public synchronized void decrement() {
        value--;
    }

    public synchronized int get() {
        return value;
    }
}

/*
explicit locking

public class Counter {
    private int value;
    private Lock lock = new ReentrantLock();

    public void increment() {
        lock.lock();
        value++;
        lock.unlock();
    }

    public void decrement() {
        lock.lock();
        value--;
        lock.unlock();
    }

    public synchronized int get() {
        return value;
    }
}
 */
