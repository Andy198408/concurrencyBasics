package synchronization.staticmethod;

public class RunPrinter {
    public static void main(String[] args) {
        //Good luck checking!!
        MyPrinter tasks = new MyPrinter();
        Thread t1 = new Thread(tasks, "first");
        Thread t2 = new Thread(tasks,"second");
        Thread t3 = new Thread(tasks,"third");

        t1.start();
        t2.start();
        t3.start();

    }
}
